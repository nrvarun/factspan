$(document).ready(function() {

    $('#sidenav-close-btn, .overlay').on('click', function() {
        $('.factspan-sidenav').toggleClass('factspan-sidenav--show');
        $('.overlay').css('display', 'none');
        $('body').css('overflow','auto');
    });

    $('.navbar-toggle').on('click', function() {
        $('.factspan-sidenav').toggleClass('factspan-sidenav--show');
        $('.overlay').css('display', 'block');
        $('body').css('overflow','hidden');
    });

/*
Testimonial section slick slider
*/
$('.factspan-testimonials__slider-container').slick({
    dots: true,
    speed: 300,
    arrows: true,
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1
});

/*
 Banner section slick slider
*/
$('.factspan-whatwedo__slider-2').slick({
    dots: true,
    autoplay: true,
    autoplaySpeed: 20000,
    speed: 500,
    arrows: false,
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.factspan-whatwedo__slider'
});

$('.factspan-whatwedo__slider').slick({
    dots: true,
    speed: 500,
    arrows: false,
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    asNavFor: '.factspan-whatwedo__slider-2'
});

/**
 * Sidenav toggling for list items
 */
$('.factspan-sidenav__links--approach, .factspan-sidenav__links--expertise').on('click', function(){
    $(this).parent().find('.factspan-sidenav__sub-links__container').toggleClass('show');
    $(this).parent().find('.factspan-sidenav__link-arrow').toggleClass('factspan-sidenav__link-arrow--rotate');
    var currentElem = $(this);
    if(currentElem.hasClass('factspan-sidenav__links--approach')){
        console.log(currentElem.next());
        if(currentElem.next().hasClass('show')){
            if($(this).parent().prev().find('.factspan-sidenav__sub-links__container')){
                $(this).parent().prev().find('.factspan-sidenav__sub-links__container').removeClass('show');
                // $(this).parent().prev().find('.factspan-sidenav__link-arrow').removeClass('factspan-sidenav__link-arrow--rotate');
            }
        }    
    }
    else {
        if(currentElem.next().hasClass('show')){
            if($(this).parent().next().find('.factspan-sidenav__sub-links__container')){
                $(this).parent().next().find('.factspan-sidenav__sub-links__container').removeClass('show');
                // $(this).parent().prev().find('.factspan-sidenav__link-arrow').removeClass('factspan-sidenav__link-arrow--rotate');
            }
        }    
    }
});

//Explore section fluid to normal container change
var resizeTimer;

$(window).on('resize', function(e) {

    clearTimeout(resizeTimer);
    resizeTimer = setTimeout(function() {
    // Run code here, resizing has "stopped"
    if($(window).width()>768){
        $('.factspan-explore').find('.container-fluid').attr('class','container');
    }
    else {
        $('.factspan-explore').find('.container').attr('class','container-fluid');
    }                
    }, 1000);

});
/**
 * Explore section - code end 
*/
      
 });